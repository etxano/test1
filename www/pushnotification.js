document.addEventListener('deviceready', function() {

	console.log("Push Notification Start ");

	FCMPlugin.getToken(function(token){
    	registerServerToken( token );
	});
	
    FCMPlugin.onTokenRefresh(function(token){
    	registerServerToken( token );
	});
	
	FCMPlugin.subscribeToTopic('all');
	
	function registerServerToken(tokenId) {
		console.log("Push Notification Id "+tokenId);
		var oldRegId = localStorage.getItem('registrationId');
		if (oldRegId !== tokenId) {
			// Save new registration ID
			localStorage.setItem('registrationId', tokenId);
			// Post registrationId to your app server as the value has changed
		}
            
        var url = HOST+"/push/device/gcm/register";
        if(device.platform=="iOS"){
            url = HOST+"/push/device/ios/register";
        }
		$.ajax({
		  type: "POST",
		   url: url,
           data: {  device_model: device.model,
                    device_identifier:device.uuid,
                    device_version:device.version,
                    device_name: window.device.name,
                    device_platform: device.platform,
                    device_serial:device.serial,
                    device_token: localStorage.getItem('registrationId'),
                    app_id: tokenId,
                    app_version: cordova.getAppVersion.getVersionNumber(),
                    app_name: cordova.getAppVersion.getAppName(),
                    apppackagename: cordova.getAppVersion.getPackageName(),
                    app_versioncode: cordova.getAppVersion.getVersionCode(),
                    app_version: cordova.getAppVersion.getVersionNumber(),
                    app_language: localStorage.getItem('language'),
                },
               }).done(function( msg ) {
                       $( "#log" ).html( msg );
                       })
                .fail(function( jqXHR, textStatus ) {
                         //alert( "Request failed: " + textStatus );
                         });
  	}
	
	FCMPlugin.onNotification(function(data){
   	console.log("Push Notification Message "+data.message);
		
        if(data.additionalData.foreground){
            //window.alert(data.title+'\n'+data.message); //This is optional as I am trying to show
            //Push message as an Alert when the app is in Foreground
            //window.alert('insidePushDeviceReady');
        }else{
            window.open(data.additionalData.redirecturl);
        }
            
        if(data.additionalData.coldstart){
            //redirecturl will be additional key in your payload to hold the relative url for redirection
            window.open(data.additionalData.redirecturl);
        }
           
	});

	
 });
